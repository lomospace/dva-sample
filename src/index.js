import dva from 'dva';
import './index.css';

// 1. Initialize
// const app = dva();

// 为了方便看ProductList 给app对象添加一些初始数据, 或在Model products里暂时添加也可以.
const app = dva(
    {
        initialState: {
            products: [
                { age: 16, name: 'Lomo168', sex: 'male', id: 1 },
                { age: 18, name: 'Lomo123', sex: 'female', id: 2 },
            ],
        }
    }
);

// 2. Plugins
// app.use({});

// 3. Model
// app.model(require('./models/example').default);
app.model(require('./models/products').default);

// 4. Router
app.router(require('./router').default);

// 5. Start
app.start('#root');
