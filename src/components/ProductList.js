import React from 'react';
import PropTypes from 'prop-types';
import {Table, Button, Popconfirm} from 'antd';


/**
 * 
 * @param {ProductList UI组件}  
 * UI 组件便于在多个页面共享使用
 */
const ProductList = ({ onDelete, products }) => {
    const columns = [{
        title: '姓名',
        dataIndex: 'name',
      }, 
      {
        title: '性别',
        dataIndex: 'sex',
      },
      {
        title: '年龄',
        dataIndex: 'age',
      },
      {
        title: 'Actions',
        render: (text, record) => {
          return (
            //   删除时只传入本行的`data-row-key`
            // onDelete(record) 即可把本行的所有数据传入
            <Popconfirm title="Delete?" onConfirm={() => onDelete(record.id)}>
              <Button>Delete</Button>
            </Popconfirm>
          );
        },
      }];

      return (
        <Table
          dataSource={products}
          columns={columns}
          rowKey="id"
        />
      );
};

/**
 * https://react.docschina.org/docs/typechecking-with-proptypes.html
 * PropTypes用来做类型校验
 */
ProductList.propTypes = {
    onDelete: PropTypes.func.isRequired,
    Products: PropTypes.array,
};

export default ProductList;