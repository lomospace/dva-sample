/**
 * ProductList的Model
 * 那么, 如何将该Model与组件ProductList component连接起来？
 * 方式: dva 提供了 connect 方法(就是 react-redux 的 connect)
 */

export default {
    namespace: 'products',  // 表示在全局 state 上的 key

    state: [
    ],  // 初始值，在这里是空数组

    // 处理异步逻辑的 effects
    effects: {},

    // 等同于 redux 里的 reducer，接收 action，同步更新 state
    // 同步更新 state 的 reducers
    reducers: {
        'delete'(state, {payload: id}) {
            // console.log(id['id']);
            // 删除state上的改行数据 来 假装删除，实际删除需要发送请求.
            return state.filter(item => item.id !== id);

            // 此处如何发送?
            // TODO
        }
    },
}