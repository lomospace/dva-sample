import React from 'react';
import {connect} from 'dva';
import ProductList from '../components/ProductList';



/**
 * 
 * @param {在这里连接ProductList组件和其对于的 product Model}  
 */
const Products = ({dispatch, products}) => {

  // payload: 参数.
  function handleDelete(id) {
    dispatch({
      type: 'products/delete', // type 里的products就是Model里的namespace名，后面的`delete`就是products Model里`reducers`下的删除delete方法
      payload: id
    })
  }

  return (
    <div>
      <h2>List of Products</h2>
      <ProductList onDelete={handleDelete} products={products}></ProductList>
    </div>
  );
}

// export default Products;
export default connect(({ products }) => ({
  products,
}))(Products);